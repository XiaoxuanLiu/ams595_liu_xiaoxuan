% Foward Elimination
function eche = rowech_1b(A)
[I,J] = size(A);
tol = eps(0.5);
i=1;
j=1;
while (i<I && j<=J)
    test=A(:,j);
    if (all(test==0))
        j=j+1;
    else
        if (abs(A(i,j))>tol)
            for k=i+1:I    % zero out process begin.
                if (abs(A(k,j))>tol)
                    A(k,:)= A(k,:) - (A(k,j)/A(i,j)) * A(i,:);
                end
            end            % zero out process end.
        else
            for r=i+1:I
                if (abs(A(r,j))>tol)
                    A([i,r],:)=A([r,i],:); %switch two rows.
                    for k=i+1:I    % zero out process begin.
                        if (abs(A(k,j))>tol)
                            A(k,:)= A(k,:) - (A(k,j)/A(i,j)) * A(i,:);                            
                        end
                    end             % zero out process end.
                    break
                end
            end
        end
        i=i+1;
        j=j+1;
    end
end
eche= A;
end
            
         
        
    