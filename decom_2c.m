function [L,U,P]= decom_2c(A)
[I,J]=size(A);
m= min(I,J);
[max_A,index]=max(A);
P=eye(I);
for i = 1:m
    k=index(i)
    A([i,k],:)= A([k,i],:)
    E=eye(I)
    E([i,k],:)= E([k,i],:)
    P= E * P
end
[U,Linv]=rowech_elem_2a(P*A)
L= inv_2b(Linv)
end
