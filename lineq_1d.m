function x = lineq_1d(A,b)
echA = rowech_1b(A);
zeroA = all(echA==0,2);  % determine whether matrix A has all-zero row.

M = [A,b'];  % get the augmented matrix.
echM = rowech_1b(M);   % turn the augmented matrix in to echelon form.
zeroM = all(echM==0,2);  % determine whether matrix M has all-zero row.

if (isequal(zeroA,zeroM))
    echM (all(echM==0,2),:) = [];
    [I,J]= size(echM);
    if (I == J-1)
        canoM = cano_1c (echM);
        x = canoM(:,J)';
    else
        x = ('Error: no solution or more than one solution.');
    end
end
disp(x);
end

     
    







