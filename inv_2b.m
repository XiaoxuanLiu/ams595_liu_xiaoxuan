function Ainv = inv(A)
[I,J]=size(A);
if (I~=J)
    Ainv='Error: input matrix should be square matrix';
else
    [eche,a]=rowech_elem_2a(A);
    [cano,b]=cano_elem_2a(eche);
    if (all(eche==0,2))
        Ainv='A has no inverse.';
    else
        Ainv= b*a;
    end
end
end