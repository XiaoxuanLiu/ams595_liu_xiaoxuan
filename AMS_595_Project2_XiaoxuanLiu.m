% AMS 595 Project2
% Xiaoxuan Liu
%====================================================
% 1.e
tim=zeros(1,10);
m= unidrnd(20,1,10);
n= unidrnd(20,1,10);
for i = 1:10
    A = unidrnd(100,m(i),n(i));
    b = unidrnd(100,1,m(i));
    tic
    lineq_1d(A,b);
    tim(i) = toc;
end
%==================================================    
 % 2.a
 % Add more computation to code 1b, get one more output P, which is the
 % product of all the elementary matrices.


