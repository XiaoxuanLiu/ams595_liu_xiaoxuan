% Backward elimination.
function Rcano = cano_1c(A)
[I,J]= size(A);
tol = eps(0.5);
piv =zeros(1,I); % save the column indicator of the first non-zero entry in every rows.

% determine whether the input matrix is echelon form.
% Step 1: find the pivot of every row, save the column number of these
%pivots one by one.

for i = 1:I
    for j=1:J
        if (abs(A(i,j))>tol)
            piv(i)= j;
            break;
        end
    end
end

% Step 2: determine whether there have all-zero rows at the bottom of the
% matrix. If there have, delete the indicator from pivot index vector.
for p = I:-1:1
    if (abs(piv(p)) <= tol)
        piv(p)=[];
    else
        break;
    end
end

% Step 3: determine whether the column numbers of each pivot is a ascending sequence. 
for m = 1:length(piv)-1
    if (piv(m) >= piv(m+1))
        Rcano = 'Error: Input should be an echelon form.';% It means the matrix inputed is not an echelon form.
        break;
    end
    Rcano = 'echelon form.';
end
% Step 4 : transform the echelon matrix to canonical form.  
if (strcmp(Rcano,'echelon form.'))
    for r = length(piv):-1:2
        A(r,:) = A(r,:)/A(r,piv(r));  %A(r,piv(m)) is the pivot of rth row and mth column.
        for m = r-1:-1:1
            A(m,:)=A(m,:)- A(m,piv(r))* A(r,:);
        end
    end
    A(1,:) = A(1,:)/A(1,piv(1));
    Rcano = A;
end
end