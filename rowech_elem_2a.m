% Foward Elimination
function [eche,L] = rowech_elem_2a(A)
[I,J] = size(A);
tol = eps(0.5);
i=1;
j=1;
L = eye(I);   % use L to save the elementary matrix.
while (i<I && j<=J)
    test=A(:,j);
    if (all(test==0))
        j=j+1;
    else
        if (abs(A(i,j))>tol)
            for k=i+1:I    % zero out process begin.
                if (abs(A(k,j))>tol)
                    coe= - (A(k,j)/A(i,j));
                    A(k,:)= A(k,:) + coe * A(i,:);
                    E = eye(I);  % define an identity matrix
                    E(k,j) = coe;  % do the elementary operations
                    L = E * L ;   % save each operation result.
                end
            end            % zero out process end.
        else
            for r=i+1:I
                if (abs(A(r,j))>tol)
                    A([i,r],:)=A([r,i],:); %switch two rows.
                    E = eye(I);     
                    E([i,r],:)=E([r,i],:);    
                    L = E * L;    
                    for k=i+1:I    % zero out process begin.
                        if (abs(A(k,j))>tol)
                            coe= - (A(k,j)/A(i,j));
                            A(k,:)= A(k,:) + coe * A(i,:);
                            E = eye(I);  
                            E(k,j) = coe;  
                            L = E * L ;   
                        end
                    end             % zero out process end.
                    break
                end
            end
        end
        i=i+1;
        j=j+1;
    end
end
eche= A;
end
            
         
        
    