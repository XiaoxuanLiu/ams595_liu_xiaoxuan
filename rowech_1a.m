function whetherRech = rowech_1a(A)
[I,J]= size(A);
tol = eps(0.5);
piv =zeros(1,I); % save the column indicator of the first non-zero entry in every rows.

% Step 1: find the pivot of every row, save the column number of these
%pivots one by one.

for i = 1:I
    for j=1:J
        if (abs(A(i,j))>tol)
            piv(i)= j;
            break;
        end
    end
end

% Step 2: determine whether there have all-zero rows at the bottom of the
% matrix. If there have, delete the indicator from pivot index vector.
for p = I:-1:1
    if (abs(piv(p)) <= tol)
        piv(p)=[];
    else
        break;
    end
end

% Step 3: determine whether the column numbers of each pivot is a ascending sequence. 
for m = 1:length(piv)-1
    if (piv(m) >= piv(m+1))
        whetherRech = 'Not echelon.';% It means the matrix inputed is not an echelon form.
        break;
    else
        whetherRech = 'echelon form.';
    end
end

% Step 4: If the matrix is an echelon form, determine whether it's a row
% canonical form.

if (strcmp(whetherRech,'echelon form.'))
    l=length(piv);
    for i = 1:l
        if (A(i,piv(i)) ~= 1)
            whetherRech = 'echelon form.';
        else
            test = A(:,piv(i));
            test(i)=[];
            if (all (test == 0))
                whetherRech = 'canonical form.';
            end
        end
    end
end


            
    
    
        

    










                    
            